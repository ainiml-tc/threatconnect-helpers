// ==UserScript==
// @name	A Different ThreatConnect
// @namespace	ThreatConnect Research
// @version	0.1
// @description	Think differently, together.
// @icon	https://image.flaticon.com/icons/png/128/478/478543.png
// @authur	Eric Jaw
// @match	*://*.threatconnect.com/*
// @include	*://*.threatconnect.com/*
// @grant	none
// @run-at	document-end
// ==/UserScript==



(function () {
    'use strict';

    function Body () { }
    function Header () { }
    function Footer () { }

    function Posts () { }
    function Analyze () { }
    function Playbooks () {
        // TODO: Check if page URL is: https://sandbox.threatconnect.com/auth/playbooks
        // TODO: turn http trigger link into href link
        // "Playbook editing is disabled when status is 'Active'. Playbook executes on URL: https://sandbox.threatconnect.com/api/playbook/d3193ba8-1d3c-4e5d-82d6-371036be33cd"

    }
    function Browse () { }
    function Spaces () { }
    function Create () { }
    function Import () { }
    function Profile () { }

})();
